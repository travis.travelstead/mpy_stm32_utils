import stm
import utime

F405_FLASH_OPTKEYR = 0x1FFFC000
F405_OPTION_DEFAULT = 0x0FFFAAED  # Based on MCU default from manual, maybe not Micropython default
F405_RESERVED_MASK = 0xF0000010  # Which bits should not be changed based on reserved status
F405_SUGGESTED_MASK = 0xFFFAAE0  # Reset values that match Micropython defaults, with BOR, start and lock bits open.
OPTKEY1 = 0x08192A3B
OPTKEY2 = 0x4C5D6E7F

BOR_LEVEL_OFF = 0x1100
BOR_LEVEL_1 = 0x1000
BOR_LEVEL_2 = 0x0100
BOR_LEVEL_3 = 0x0000

BOR_MASK = 0xFFFFFFF0  # Leaves first nib clear to avoid BOR settings write start bits


def read_BOR():
    bor = stm.mem32[F405_FLASH_OPTKEYR] & 0b1100
    bor1 = stm.mem32[F405_FLASH_OPTKEYR]
    o1 = stm.mem32[F405_FLASH_OPTKEYR]
    o2 = stm.mem32[F405_FLASH_OPTKEYR + 8]
    bor2 = o1 & 0xffff | (o2 & 0xffff) << 16
    print('{:032b}'.format(bor1))
    print('{:032b}'.format(bor2))
    print('{:032b}'.format(F405_OPTION_DEFAULT))


def read_BOR_BIN():
    bin(read_BOR)


def set_bor(bor_level=BOR_LEVEL_3):
    # Get existing options
    existing_options = stm.mem32[F405_FLASH_OPTKEYR]
    print('Existing Options: {}'.format('{:032b}'.format(existing_options)))  # Higher bytes I am not sure are actual options settings.
    new_options = F405_SUGGESTED_MASK | bor_level
    print('     New Options: {}'.format('{:032b}'.format(new_options)))

    print('Start update of options')
    utime.sleep_ms(2000)

    # Do nothing else but update, no prints. USB will to be lost, and it does not complete correctly.

    # unlock
    stm.mem32[stm.FLASH + stm.FLASH_OPTKEYR] = OPTKEY1
    stm.mem32[stm.FLASH + stm.FLASH_OPTKEYR] = OPTKEY2

    # check BSY in FLASH_SR
    while stm.mem32[stm.FLASH + stm.FLASH_SR] & (1 << 16):
        pass

    # write value to FLASH_OPTCR
    stm.mem32[stm.FLASH + stm.FLASH_OPTCR] = new_options

    # set OPTSTRT in FLASH_OPTCR
    stm.mem32[stm.FLASH + stm.FLASH_OPTCR] |= 0b10

    # wait for BSY to clear
    while stm.mem32[stm.FLASH + stm.FLASH_SR] & (1 << 16):
        pass

    # lock
    stm.mem32[stm.FLASH + stm.FLASH_OPTCR] |= 0b01



# def read_option(option_mem):
#     o1 = stm.mem32[option_mem]
#     o2 = stm.mem32[option_mem + 8]
#     return o1 & 0xffff | (o2 & 0xffff) << 16

# def write_option(value):
#     # unlock
#     stm.mem32[stm.FLASH + stm.FLASH_OPTKEYR] = OPTKEY1
#     stm.mem32[stm.FLASH + stm.FLASH_OPTKEYR] = OPTKEY2
#     # check BSY in FLASH_SR
#     while stm.mem32[stm.FLASH + stm.FLASH_SR] & (1 << 16):
#         pass
#     # write value to FLASH_OPTCR
#     stm.mem32[stm.FLASH + stm.FLASH_OPTCR] = value
#     # set OPTSTRT in FLASH_OPTCR
#     stm.mem32[stm.FLASH + stm.FLASH_OPTCR] |= 1 << 1
#     # wait for BSY to clear
#     while stm.mem32[stm.FLASH + stm.FLASH_SR] & (1 << 16):
#         pass
#     # lock
#     stm.mem32[stm.FLASH + stm.FLASH_OPTCR] |= 1



#     # read initial options
# o = read_option(OPTION_F4)
# print(hex(o))

# # set level 3 BOR
# o = o & ~0xc | 0x00

# # change the options
# write_option(o)

# # read new options
# o = read_option(OPTION_F4)
# print(hex(o))






#  /*---------------------------------------------------------------------------*/
#   271 /** @brief Program the Option Bytes
#   272
#   273 This performs all operations necessary to program the option bytes.
#   274 The option bytes do not need to be erased first.
#   275
#   276 @param[in] data value to be programmed.
#   277 */
#   278
#   279 void flash_program_option_bytes(uint32_t data)
#   280 {
#   281         flash_wait_for_last_operation();
#   282
#   283         if (FLASH_OPTCR & FLASH_OPTCR_OPTLOCK) {
#   284                 flash_unlock_option_bytes();
#   285         }
#   286
#   287         FLASH_OPTCR = data & ~0x3;
#   288         FLASH_OPTCR |= FLASH_OPTCR_OPTSTRT;  /* Enable option byte prog. */
#   289         flash_wait_for_last_operation();
#   290 }
#   291 /**@}*/