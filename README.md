# mpy_stm32_utils


## BOR set

### Programming user option bytes
To run any operation on this sector, the option lock bit (OPTLOCK) in the Flash option control register (FLASH_OPTCR) must be cleared. To be allowed to clear this bit, you have to perform the following sequence:
1. Write OPTKEY1 = 0x0819 2A3B in the Flash option key register (FLASH_OPTKEYR)
2. Write OPTKEY2 = 0x4C5D 6E7F in the Flash option key register (FLASH_OPTKEYR)
The user option bytes can be protected against unwanted erase/program operations by setting the OPTLOCK bit by software.

### Modifying user option bytes on STM32F405xx/07xx and STM32F415xx/17xx
To modify the user option value, follow the sequence below:
1. Check that no Flash memory operation is ongoing by checking the BSY bit in the FLASH_SR register
2. Write the desired option value in the FLASH_OPTCR register.
3. Set the option start bit (OPTSTRT) in the FLASH_OPTCR register
4. Wait for the BSY bit to be cleared.
The value of an option is automatically modified by first erasing the user configuration sector and then programming all the option bytes with the values contained in the FLASH_OPTCR register.


https://github.com/blacksphere/blackmagic/blob/master/src/target/stm32f4.c
http://libopencm3.org/docs/latest/stm32f4/html/flash__common__f24_8c_source.html

